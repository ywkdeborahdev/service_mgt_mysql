const express = require("express");
const mysql = require('mysql2');
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const multer = require("multer");
const fs = require('fs');
const shortid = require('shortid');
const { query } = require("express");

const app = express();
const upload = multer({ dest: "uploaded-photos/" })
let pool = null;
const saltRounds = 10;
let jwtSecret = "deb";

app.use(express.json());
app.use("/uploaded-photos", express.static("uploaded-photos/"));

function getJWT(payload) {
    const options = {
        expiresIn: "3600s"
    }
    return jwt.sign(payload, jwtSecret, options);
}

function auth(req, res, next) {
    if (!req.headers.authorization) {
        return res.json({
            success: false,
            message: "please login"
        })
    }
    const authorizationHeader = req.headers.authorization;
    const splittedAuth = authorizationHeader.split(" ");

    if (splittedAuth[0] === "Bearer") {
        let token = splittedAuth[1];
        jwt.verify(token, jwtSecret, function (err, decoded) {
            if (err) {
                res.status(401);
                return res.json({
                    success: false,
                    message: "please login again"
                })
            }
            req.tokenPayload = decoded;
            next();
        });
    }
}

async function getServiceFromDB(paramServiceId) {

    try {
        const [result, fields] = await pool.execute('select * from services where serviceId =?', [paramServiceId]);

        if (result.length === 0) {
            return {
                success: false,
                message: "no such service"
            };
        } else {
            return result[0];
        }

    } catch (error) {
        return {
            success: false,
            error: true
        };
    }


}

function checkIfOwner(tokenUser, serviceOwner) {
    if (tokenUser !== serviceOwner) {
        return {
            success: false,
            message: "only owner can perform this action"
        }
    } else {
        return 1;
    }
}

function checkIfNotOwner(tokenUser, serviceOwner) {
    console.log(tokenUser, serviceOwner)
    if (tokenUser === serviceOwner) {
        return {
            success: false,
            message: "owner cannot do booking"
        }
    } else {
        return 1;
    }
}

async function checkMemberExist(username) {

    try {
        const [result, fields] = await pool.execute('select * from members where username =?', [username]);
        return result;

    } catch (error) {
        return { error: true };
    }

}

function pictureFieldFormat(data) {
    let result = data.reduce((out, item) => {

        let {
            serviceId,
            serviceOwner,
            name,
            description,
            availability,
            likeCount,
            pictureId,
            imagePath
        } = item;

        //Separate the "id" and "everything else"

        let newObj = {
            serviceId,
            serviceOwner,
            name,
            description,
            availability,
            likeCount
        }

        if (pictureId === null) {

            newObj.pictures = [];
            out.push(newObj);
            return out;
        }


        let pictureObj = {
            pictureId,
            imagePath
        }

        let existing = out.find(service =>
            service.serviceId === item.serviceId);


        if (existing) {
            existing.pictures.push(pictureObj);
            // console.log(existing);
        } else {
            newObj["pictures"] = [pictureObj];
            //delete picture ID and image Path property
            out.push(newObj);
        }

        return out;
    }, []);
    console.log(result)
    return result;
}

function formatDate(newDate) {

    function addZero(num) {
        let str = num.toString();
        if (str.length === 1) {
            return "0" + str;
        }
        return str;
    }
    let year = newDate.getFullYear();
    let month = addZero(newDate.getMonth() + 1);
    let date = addZero(newDate.getDate());
    let hour = addZero(newDate.getHours());
    let minute = addZero(newDate.getMinutes());
    let second = addZero(newDate.getSeconds());

    return `${year}-${month}-${date} ${hour}:${minute}:${second}`;
}


//register
app.post("/register", async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;


    //if exist member, ask user to input another name
    let checkExistMember = await checkMemberExist(username);

    if (checkExistMember.length > 0 || checkExistMember.error) {
        return res.json({
            success: false,
            message: "This name has been registered. please use another name",
        })

    };

    //hash password
    bcrypt.hash(password, saltRounds, function (err, hash) {
        if (err) {
            return res.json({
                success: false,
                message: "please register again"
            })
        }


        (async () => {
            const sqlQuery = 'insert into members(username,password) values (?, ?)';
            const [result, fields] = await pool.execute(sqlQuery, [username, hash]);

            //generate Token
            let token = getJWT({
                username: username
            })

            return res.json({
                token,
                result

            })
        })();
    });
})

//login
app.post("/login", async (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    let matchedMember = await checkMemberExist(username);
    console.log(matchedMember);
    console.log(matchedMember);

    if (matchedMember.length === 0 || matchedMember.error) {
        return res.json({
            success: false,
            message: "no such member"
        })
    }

    bcrypt.compare(password, matchedMember[0].password, function (err, result) {
        // result == true
        if (result) {

            let token = getJWT({
                username: username
            })

            return res.json({
                token
            })
        } else {
            console.log(err)
            res.json({
                success: false,
                message: "wrong password"
            })
        }
    });
})

//post new service
app.post("/service", auth, async (req, res) => {

    const owner = req.tokenPayload.username;
    const name = req.body.name;
    const description = req.body.description;

    try {
        const sqlQuery = 'insert into services (serviceId, serviceOwner, name, description, availability, likeCount, isDeleted) values (?, ?, ?, ?, ?, ?, ?)';
        const data = [shortid.generate(), owner, name, description, 1, 0, 0];
        const [result, fields] = await pool.execute(sqlQuery, data);
        return res.json({
            success: true,
            result: result
        })

    } catch (error) {
        console.log(error)
        return res.json({ error: true });
    }

})

//post photos
app.post("/service/:serviceId/photos", auth, upload.array("photos", 10), async (req, res) => {
    // check if owner , body should include service id
    if (!req.files) {
        return res.json({
            success: false,
            message: "upload fail"
        });
    }

    // check if service exist
    let service = await getServiceFromDB(req.params.serviceId);
    if (service.hasOwnProperty("success")) {
        return res.json(service);
    }

    //check if owner
    let checkOwnerResult = checkIfOwner(req.tokenPayload.username, service.serviceOwner)
    if (checkOwnerResult.hasOwnProperty("success")) {
        return res.json(checkOwnerResult);
    }

    let pictures = req.files.map(file => file.path);
    let sqlResult = []

    console.log(pictures);
    for (let picture of pictures) {

        try {
            const sqlQuery = 'insert into pictures (imagePath, serviceId) values (?, ?)';
            // const sqlQuery = 'insert into pictures (imagePath, serviceId) values (?, ?),(?,?),(?,?)';
            const data = [picture, req.params.serviceId]
            const [result, fields] = await pool.execute(sqlQuery, data);
            sqlResult.push(result);
        } catch (error) {
            return res.json({ error: true });
        }
    }

    return res.json({
        success: true,
        result: sqlResult
    })

})

//get all services
app.get("/service", async (req, res) => {

    try {
        const sqlQuery = `
            select s.serviceId, serviceOwner, name, description, availability, likeCount, pictureId, imagePath, b.isValid
            from services s
            left join bookings b
            on s.serviceId = b.serviceId
            left join pictures p
            on s.serviceId = p.serviceId
            where b.isValid = 1
        `;
        const [result, fields] = await pool.execute(sqlQuery);
        console.log(result);
        let formattedResult = pictureFieldFormat(result);
        return res.json({
            success: true,
            result: formattedResult
        })

    } catch (error) {
        console.log(error)
        return res.json({ error: true });
    }

})

//get one service 
app.get("/service/:serviceId", async (req, res) => {
    // check if service exist
    let service = await getServiceFromDB(req.params.serviceId);
    if (service.hasOwnProperty("success")) {
        return res.json(service);
    }

    try {
        const sqlQuery = `
            select s.serviceId, serviceOwner, name, description, availability, likeCount, pictureId, imagePath, b.isValid
            from services s
            left join bookings b
            on s.serviceId = b.serviceId
            left join pictures p
            on s.serviceId = p.serviceId
            where s.serviceId = ?
        `;
        const data = [req.params.serviceId]
        const [result, fields] = await pool.execute(sqlQuery, data);
        console.log(result);
        let formattedResult = pictureFieldFormat(result);
        return res.json({
            success: true,
            result: formattedResult
        })

    } catch (error) {
        console.log(error);
        return res.json({ error: true });
    }

})

//toggle availability
app.patch("/service/:serviceId/toggleAvailability", auth, async (req, res) => {
    // check if service exist
    let service = await getServiceFromDB(req.params.serviceId);
    if (service.hasOwnProperty("success")) {
        return res.json(service);
    }


    //check if owner
    let checkOwnerResult = checkIfOwner(req.tokenPayload.username, service.serviceOwner)
    if (checkOwnerResult.hasOwnProperty("success")) {
        return res.json(checkOwnerResult);
    }

    let updateStatus;
    if (service.availability) {
        updateStatus = 0;
    } else {
        updateStatus = 1;
    };

    try {
        const sqlQuery = 'update services set availability = ? where serviceId = ?';
        const data = [updateStatus, req.params.serviceId];
        const [result, fields] = await pool.execute(sqlQuery, data);
        return res.json({
            success: true,
            message: result
        })
    } catch (error) {
        return res.json({ error: true });
    }

})

//update One Service
app.patch("/service/:serviceId", auth, async (req, res) => {
    // check if service exist
    let service = await getServiceFromDB(req.params.serviceId);
    if (service.hasOwnProperty("success")) {
        return res.json(service);
    }


    //check if owner
    let checkOwnerResult = checkIfOwner(req.tokenPayload.username, service.serviceOwner)
    if (checkOwnerResult.hasOwnProperty("success")) {
        return res.json(checkOwnerResult);
    }

    if (!req.body.updateDetail.hasOwnProperty("name") && !req.body.updateDetail.hasOwnProperty("description")) {
        return res.json({
            success: false,
            message: "nothing to update"
        });
    }

    let query = [];
    let params = [];
    if (req.body.updateDetail.hasOwnProperty("name")) {
        query.push("name=?");
        params.push(req.body.updateDetail.name);
    }
    if (req.body.updateDetail.hasOwnProperty("description")) {
        query.push("description=?");
        params.push(req.body.updateDetail.description);
    }
    params.push(req.params.serviceId);
    console.log(params);

    try {
        const sqlQuery = `update services set ${query.join(",")} where serviceId = ?`;
        const data = params;
        const [result, fields] = await pool.execute(sqlQuery, data);
        return res.json({
            success: true,
            message: result
        })
    } catch (error) {
        console.log(error);
        return res.json({ error: true });
    }

})

//like
app.patch("/service/:serviceId/like", auth, async (req, res) => {
    // check if service exist
    let service = await getServiceFromDB(req.params.serviceId);
    if (service.hasOwnProperty("success")) {
        return res.json(service);
    }

    try {
        const sqlQuery = 'update services set likeCount = likeCount + 1 where serviceId = ?';
        const data = [req.params.serviceId]
        const [result, fields] = await pool.execute(sqlQuery, data);
        return res.json({
            success: true,
            message: result
        })
    } catch (error) {
        return res.json({ error: true });
    }

})

//comment
app.post("/service/:serviceId/comment", auth, async (req, res) => {
    // check if service exist
    let service = await getServiceFromDB(req.params.serviceId);
    if (service.hasOwnProperty("success")) {
        return res.json(service);
    }

    if (!req.body.content) {
        return res.json({
            success: false,
            message: "please input comments"
        });
    }

    try {
        const sqlQuery = `
            insert into comments (author, content, createDateTime, serviceId)
            values (?, ?, ?, ?)
        `;
        const data = [
            req.tokenPayload.username,
            req.body.content,
            formatDate(new Date()),
            req.params.serviceId
        ];
        const [result, fields] = await pool.execute(sqlQuery, data);
        return res.json({
            success: true,
            message: result
        })
    } catch (error) {
        console.log(error);
        return res.json({ error: true });
    }

})

//book
app.post("/service/:serviceId/book", auth, async (req, res) => {
    // check if service exist
    let service = await getServiceFromDB(req.params.serviceId);
    if (service.hasOwnProperty("success")) {
        return res.json(service);
    }

    //check if owner
    let checkNotOwnerResult = checkIfNotOwner(req.tokenPayload.username, service.serviceOwner)
    if (checkNotOwnerResult.hasOwnProperty("success")) {
        return res.json(checkNotOwnerResult);
    }

    //check if service is booked


    try {
        const sqlQuery = `
            select 1 from bookings
            where serviceId = ? AND isValid = 1;
        `;
        const data = [req.params.serviceId]
        const [result, fields] = await pool.execute(sqlQuery, data);
        console.log(result)
        if (result.length > 0) {
            return res.json({
                success: false,
                message: "service is booked"
            })
        }
    } catch (error) {
        console.log(error);
        return res.json({ error: true });
    }


    //insert booking to bookings table
    try {
        const sqlQuery = `
            insert into bookings (username, bookingDateTime, serviceId, isValid)
            values (?, ?, ?, 1);
        `;
        const data = [req.tokenPayload.username, formatDate(new Date()), req.params.serviceId];
        const [result, fields] = await pool.execute(sqlQuery, data);

        return res.json({
            success: true,
            message: result
        })

    } catch (error) {
        console.log(error);
        return res.json({ error: true });
    }

})

//remove booking
app.patch("/service/:serviceId/remove-booking", auth, async (req, res) => {
    // check if service exist
    let service = await getServiceFromDB(req.params.serviceId);
    if (service.hasOwnProperty("success")) {
        return res.json(service);
    }

    //check if owner
    let checkOwnerResult = checkIfOwner(req.tokenPayload.username, service.serviceOwner)
    if (checkOwnerResult.hasOwnProperty("success")) {
        return res.json(checkOwnerResult);
    }

    try {
        const sqlQuery = `
            select 1 from bookings
            where serviceId = ? or (serviceId = ? AND isValid = 1)
        `;
        const data = [req.params.serviceId, req.params.serviceId]
        const [result, fields] = await pool.execute(sqlQuery, data);

        console.log(result)

        if (result.length === 0) {
            return res.json({
                success: false,
                message: "it is not booked"
            })
        }

        // update booking if it is valid

        const sqlQuery2 = `
            UPDATE bookings
            SET isValid = 0
            WHERE serviceId=?
        `;
        const data2 = [req.params.serviceId];
        const [result2, fields2] = await pool.execute(sqlQuery2, data2);

        return res.json({
            success: true,
            message: result2
        })

    } catch (error) {
        console.log(error);
        return res.json({ error: true });
    }
})

//delete service
app.patch("/service/:serviceId/delete", auth, async (req, res) => {
    // check if service exist
    let service = await getServiceFromDB(req.params.serviceId);
    if (service.hasOwnProperty("success")) {
        return res.json(service);
    }
    //check if owner
    let checkOwnerResult = checkIfOwner(req.tokenPayload.username, service.serviceOwner)
    if (checkOwnerResult.hasOwnProperty("success")) {
        return res.json(checkOwnerResult);
    }

    try {
        const sqlQuery = `
            select 1 from bookings
            where serviceId = ? and isValid = 1
        `;
        const data = [req.params.serviceId]
        const [result, field] = await pool.execute(sqlQuery, data);
        console.log(result)
        if (result.length > 0) {
            return res.json({
                success: false,
                message: "already booked, cannot delete now"
            })
        }

        const sqlQuery2 = `
            select imagePath
            from pictures
            where serviceId = ?
        `;

        const [result2, field2] = await pool.execute(sqlQuery2, data);
        console.log(result2)
        for (let filePath of result2) {

            fs.unlink(filePath.imagePath, (err => {
                if (err)
                    return res.json({
                        success: false,
                        message: "failed to delete files"
                    })
                else {
                    console.log(`\nDeleted file: ${filePath.imagePath}`);
                }
            }));
        }

        const sqlQuery3 = `
            delete from pictures
            where serviceId = ?        
        `
        const [result3, field3] = await pool.execute(sqlQuery3, data);
        console.log("deleted picture record: ", result3);

        const sqlQuery4 = `
            update services
            set isDeleted = 1
            where serviceId = ?
        `;
        const [result4, field4] = await pool.execute(sqlQuery4, data);
        return res.json({
            success: true,
            message: "service deleted"
        })
    } catch (error) {
        console.log(error);
        return res.json({ error: true });
    }
})

//test endpoint

app.get("/endpoint", async (req, res) => {
    // try {
    //     const [result, fields] = await pool.query('select * from member');
    //     res.json(result)
    // } catch (error) {
    //     res.json({ error: true })
    // }

    return res.json(await getServiceFromDB("deb"));
});




app.listen(3000, () => {
    console.log("Example app listening on port 3000!");
    pool = mysql.createPool({
        host: 'localhost',
        port: 3306,
        user: 'root',
        password: 'root',
        database: 'assignment2_db'
    }).promise();
});